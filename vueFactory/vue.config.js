const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const renameOutputPlugin = require('rename-output-webpack-plugin');

if(process.env.npm_lifecycle_event=='build'){
	module.exports = {
	  configureWebpack: {
	    output: {
		    path: path.resolve(__dirname, './dist/'),
		    filename: '../../public/js/lib/[name].js',
		    chunkFilename:'../../public/js/lib/[id].js'
		},
		plugins: [
			new ExtractTextPlugin({
		      filename: '../../public/css/lib/[name].css',
		      allChunks: true,
		    }),
		    new webpack.optimize.CommonsChunkPlugin({
		      name: '@vue2-vendor',
		      minChunks (module) {
		        // any required modules inside node_modules are extracted to vendor
		        return (
		          module.resource &&
		          /\.js$/.test(module.resource) &&
		          module.resource.indexOf(
		            path.join(__dirname, './node_modules/')
		          ) === 0
		        )
		      }
		    }),
		    // extract webpack runtime and module manifest to its own file in order to
		    // prevent vendor hash from being updated whenever app bundle is updated
		    new webpack.optimize.CommonsChunkPlugin({
		      name: '@vue1-manifest',
		      minChunks: Infinity
		    }),
		    new renameOutputPlugin({
		    	'manifest':'../../public/js/lib/@vue1-manifest.js',
		    	'vendor':'../../public/js/lib/@vue2-vendor.js',
		    	'app':'../../public/js/lib/@vue3-app.js',
		    })
		],
		resolve: {
		    alias: {
		      'vue$': 'vue/dist/vue.esm.js',
		    }
		  },
	  	}
	}
}