import Vue from 'vue'
import testBtn from './components/testBtn'
import flipCardComponent from './components/flipCardComponent'

Vue.config.productionTip = false

window.Vue = Vue;
let vueFactory = window.vueFactory||{} 
vueFactory.testBtn=function(){
	Vue.component('test-btn',testBtn);
}
vueFactory.flipCardComponent=function(){
	Vue.component('flip-card',flipCardComponent);
}

// import App from './App'
let App = App||null;
if(App){
	vueFactory.flipCardComponent();
	new Vue({
	  render: h => h(App)
	}).$mount('#app')
}

window.vueFactory = vueFactory;